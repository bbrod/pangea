package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var rootCmd = &cobra.Command{
	Use:   "pangea",
	Short: "Pangea is a application generator for clouds",
	Long: `A Fast and Flexible Application Generator that's
	allow to write distributed applications as a task groups`,
}

// Execute executes the root command.
func Execute() {
	rootCmd.Execute()
}

func init() {
	rootCmd.PersistentFlags().StringP("author", "a", "David Bérichon", "Author name for copyright attribution")
	viper.SetDefault("author", "David Bérichon <davidberich@gmail.com>")
}
